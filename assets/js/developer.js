var url = new URL(window.location)
var days = parseInt(url.searchParams.get("days"))
var showCats = url.searchParams.get('showCats')
var cats = [
    'https://icatcare.org/app/uploads/2018/07/Thinking-of-getting-a-cat.png',
    'https://www.petmd.com/sites/default/files/styles/breeds_image/public/wideeyed-tortoiseshell-tabby-cat-about-to-pounce-on-something-picture-id489590895.jpg?itok=4ar_bDfG',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR0xU7niA9lJHSI5iJgPaR8Cjgy7L4rwiYf2YhATXqcNYDiWoKhPxYQJmlEBdfpLZ6UXMk&usqp=CAU',
    'https://static01.nyt.com/images/2020/04/22/science/22VIRUS-PETCATS1/22VIRUS-PETCATS1-mediumSquareAt3X.jpg',
    'https://api.time.com/wp-content/uploads/2015/02/cats.jpg?quality=85&w=1024&h=512&crop=1',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTJh5kgOqt5xacw9IhGakSqpdNDcvjhq6qiLGcfEu-u2Q8OeazkXIh_tLZx0F0ue5eGIk&usqp=CAU'
]
if (showCats) {
    var i = 0
    $(".posts-section img").each(function () {
        $(this).attr('src', cats[i])
        i++
    })
}

$("#released-days").val(days)
if (days > 0) $("#counter").removeClass("d-none")
if (days === 0) $("#time-out").removeClass("d-none")
if (days === -1) $("#not-yet").removeClass("d-none")
if (days === -2) $("#no-information").removeClass("d-none")